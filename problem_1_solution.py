from abc import ABC, abstractmethod
from collections import defaultdict


class Mapper(ABC):
    def _map(self, data_part):
        pass


class Reducer(ABC):
    def _reduce(self, kvs):
        pass


class MapReduce(Mapper, Reducer):
    def __init__(self, mapper, reducer):
        self.mapper = mapper()
        self.reducer = reducer()

    def runMR(self, data_parts):
        storage = defaultdict(list)
        for data_part in data_parts:
            for key, value in self.mapper._map(data_part):
                storage[key].append(value)
        return self.reducer._reduce(storage)


#####################################################################

data = ["Handshake lets universities and employers connect with a single click",
        "leading to more diverse, high-quality networking opportunities for students",
        "and employers. Because Handshake now connects over 300,000 unique employers",
        "from every industry and region, most schools see a 2-3x increase in relevant",
        "job opportunities within the first 6 months of switching to Handshake"
        ]


class WCMapper(Mapper):
    def _map(self, data_part):
        words = data_part.split()
        word_dic = {}
        for word in words:
            if word in word_dic:
                word_dic[word] += 1
            else:
                word_dic[word] = 1

        output = []
        for key in word_dic.keys():
            output.append((key, word_dic[key]))
        return output


class WCReducer(Reducer):
    def _reduce(self, kvs):
        # TODO: reducer code here
        output = []
        for key in kvs.keys():
            values = kvs[key]
            sum = 0
            for value in values:
                sum += value

            output.append((key, sum))
        return output


def main():
    for x in MapReduce(WCMapper, WCReducer).runMR(data):
        print("'{}': {}".format(x[0], x[1]))


if __name__ == "__main__":
    # execute only if run as a script
    main()
