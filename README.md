# First problem

### Implement word count map reduce

The code in [problem_1_start.py](./problem_1_start.py) is the basis
for a map/reduce implementation.

Fill in the code for WCMapper::_map and WCReducer::_reduce to perform
a word count given the input

### Expected output

```text
'Handshake': 3
'lets': 1
'universities': 1
'and': 3
'employers': 2
'connect': 1
'with': 1
'a': 2
'single': 1
'click': 1
'leading': 1
'to': 2
'more': 1
'diverse,': 1
'high-quality': 1
'networking': 1
'opportunities': 2
'for': 1
'students': 1
'employers.': 1
'Because': 1
'now': 1
'connects': 1
'over': 1
'300,000': 1
'unique': 1
'from': 1
'every': 1
'industry': 1
'region,': 1
'most': 1
'schools': 1
'see': 1
'2-3x': 1
'increase': 1
'in': 1
'relevant': 1
'job': 1
'within': 1
'the': 1
'first': 1
'6': 1
'months': 1
'of': 1
'switching': 1
```

### Bonus
the solution can be written completely using comprehensions

 
 
# Second problem
The Handshake engineering team has been tasked with sending n batches of emails in the coming d days, where n=len(batches).  Each batch i (0<=i<n) contains a specific number of emails equal to batches[i]. The emails are enqueued and sent daily.

The goal of the exercise is to write a function int optimalLength(int d, int batches[]) that computes the minimal length of the queue necessary to send the emails.

Batches must be sent in order and there are no partial batches. The size of a batch may not be zero. 

Example: d = 2, batches = [ 3000, 1005, 2000 ] 

There are 3 batches to send. The minimal queue length is 3005 as the optimal way to send the emails is:

Day 1 : enqueue and send batch #0 : size needed: 3000

Day 2:  enqueue and send batch #1: size needed: 1005+2000 = 3005



